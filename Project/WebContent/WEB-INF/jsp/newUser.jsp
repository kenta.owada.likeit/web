<%@ page language="java" contentType="text/html; charset=UTF-8" %>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="css/newUser.css">
</head>
<body>
	<div class=header>
		<div class="container">
			<div class="form-group row">
				<p class=user>ユーザー名 さん</p>
				<a href="LoginServlet" class="logout">ログアウト</a>
			</div>
		</div>
	</div>

	<div class="main">
		<div class="container">
			<h1>ユーザ新規登録</h1>

			<p class="err">${err}</p>



			<form action="NewUser" method="post">
				<div class="form-group row">
					<p>ログインID</p>
					<div class="col-sm-2">
						<input type="text" name="login_id" class="form-control">
					</div>
				</div>



				<div class="form-group row">
					<p>パスワード</p>
					<div class="col-sm-2">
						<input type="password" name="password" class="form-control">
					</div>
				</div>



				<div class="form-group row">
					<p>パスワード（確認）</p>
					<div class="col-sm-2">
						<input type="password" name="password_con"class="form-control">
					</div>
				</div>




				<div class="form-group row">
					<p>ユーザ名</p>
					<div class="col-sm-2">
						<input type="type" name="name" class="form-control">
					</div>
				</div>



				<div class="form-group row">
					<p>生年月日</p>

					<div class="col-sm-2">
						<input type="date"  name="birth_date" class="form-control">
					</div>
				</div>


				<input type="submit" value="登録">
			</form>
			<br>

		</div>
	</div>
	<div class="container">
		<a href="UserListServlet" class="btn">戻る</a>
	</div>
</body>
</html>