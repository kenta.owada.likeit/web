package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;

/**
 * Servlet implementation class NewUser
 */
@WebServlet("/NewUser")
public class NewUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO Auto-generated method stub

		//newUserから受け取っただーたをセット
		request.setCharacterEncoding("UTF-8");
		String loginid = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordcon = request.getParameter("password_con");
		String name = request.getParameter("name");
		String birthdate = request.getParameter("birth_date");

		//--------------------nullがある場合ーーーーーーーーーーーーーーーーーーーーーーーーー
		if (loginid.equals(null) && password.equals(null) && passwordcon.equals(null) && name.equals(null)
				&& birthdate.equals(null)) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
		}

		//--------------------パスワードエラーメッセージーーーーーーーーーーーーーーーーーーーーー
		if (password.equals(passwordcon) == false) {
			String err = "ログインIDまたはパスワードが異なります";
			request.setAttribute("err", err);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
		}

		//-------------------　暗号化 ------------------------------------------------------------------　
		String source = password;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		//-------------------------------------------------------------------------------------------
		password = result;

		UserDao userDao = new UserDao();
		userDao.addUserInfo(loginid, password, name, birthdate);
		response.sendRedirect("UserListServlet");

	}

}
