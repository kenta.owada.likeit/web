package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.User;

public class UserDao {
	public User findLoginInfo(String login_id, String password) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE login_Id=? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			pStmt.setString(2, password);
			//rsにSQLの実行結果が入ってる
			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");

			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void addUserInfo(String loginid, String password, String name, String birthdate) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;

		try {
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			String str = sdf.format(timestamp);

			// データベースへ接続
			conn = DBManager.getConnection();
			// INSERT文を準備
			String sql = "INSERT INTO user (login_id, name,birth_date,password,create_date,update_date) VALUES (?,?,?,?,?,?)";
			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginid);
			pStmt.setString(2, name);
			pStmt.setString(3, birthdate);
			pStmt.setString(4, password);
			pStmt.setString(5, str);
			pStmt.setString(6, str);

			int count = pStmt.executeUpdate();
			System.out.println(count);

		//------------------追加データ出力-----------------------------------------
			System.out.println(loginid);
			System.out.println(name);
			System.out.println(birthdate);
			System.out.println(password);
			System.out.println(str);
			System.out.println("成功！");
		//------------------------------------------------------------

		} catch (SQLException e) {
			e.printStackTrace();
			// 後で消す
			System.out.println("しっぱい");
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public  User  findAll(){
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT login_id,name,birth_date FROM user";

			Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date  birthDate = rs.getDate("birth_date");

			return new User(loginIdData,nameData,birthDate);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
